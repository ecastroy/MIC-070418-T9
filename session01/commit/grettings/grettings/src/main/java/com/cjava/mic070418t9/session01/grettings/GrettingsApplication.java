package com.cjava.mic070418t9.session01.grettings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrettingsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrettingsApplication.class, args);
		//
	}
}
