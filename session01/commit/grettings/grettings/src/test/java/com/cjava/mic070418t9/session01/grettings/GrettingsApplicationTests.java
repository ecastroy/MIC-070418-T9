package com.cjava.mic070418t9.session01.grettings;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GrettingsApplicationTests {

	@Test
	public void contextLoads() {
	}

}
